'''
Created on Aug 18, 2016

@author: Earth
'''

import csv
from audioop import reverse

potholes_by_block = {}

f = open('potholes.csv')
for row in csv.DictReader(f):
    status = row['STATUS']
    if status != 'Open':
        continue
    try:
        addr = row['STREET ADDRESS']
    except ValueError:
        continue
    
    try:
        parts = addr.split()
        n = parts[0]
    except:
        continue
    
    parts[0] = n[:-2] + 'XX'
        
    try:
        num = int(row['NUMBER OF POTHOLES FILLED ON BLOCK'])
    except:
        num = 0
    
    block = ' '.join(parts)
    
    if(block in potholes_by_block):
        potholes_by_block[block] += num
          
    else:
        potholes_by_block[block] = num
  
# for key, value in potholes_by_block.items() :
#     print (key, value)

import operator
ans = sorted(potholes_by_block.items(), key=operator.itemgetter(1), reverse=True)
for i in range(0, 5):
    print(ans[i])
    